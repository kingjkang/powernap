﻿<#
powerNAP module
#>

$domain = "corp.local"
$vCenterAdminAccount = "administrator"
$NSXAdminAccount = "administrator"
$ESXiAdminAccount = "root"
$pathtomodules = "who knows"
$vcsa1 = "vcsa-01a"
$vcsa2 = "vcsa-01a"
$vcsa3 = "vcsa-01a"
$vcsa4 = "vcsa-01a"
$nsxmgr1 = "nsx-01a"
$nsxmgr2 = "nsx-01a"
$nsxmgr3 = "nsx-01a"
$nsxmgr4 = "nsx-01a"

$global:vccreds = $null
$global:nsxcreds = $null
$global:esxihostcreds = $null

function powernap{
    Write-Host "BRB taking a powerNAP"
    Write-Host "contains functions for your siesta `n"
    Write-Host "init"
    Write-Host "servers"
    Write-Host "cacheCredentials"
    Write-Host "clearCredentials"
    Write-Host "vcc"
    Write-Host "vcd"
    Write-Host "nsxc"
    Write-Host "nsxd"
}

function cacheCredentials{
    $selection = $null
    while(($selection -ne "1") -and ($selection -ne "2") -and ($selection -ne "3") -and ($selection -ne "4") -and ($selection -ne "q")){
        Write-Host "1 to cache vCenter 1 credentials"
        Write-Host "2 to cache NSX Manager 1 credentials"
        Write-Host "3 to cache ESXi Host credentials"
        Write-Host "4 to cache All credentials"
        Write-Host "q to quit"
        $selection = Read-Host "Select which credentials you would like to cache"
    }
    if($selection -eq "1"){
        Write-Host "Caching vCenter 1 credentials"
        $global:vccreds = Get-Credential -Message "Admin account to connect to VCSA 1" -UserName "$($vCenterAdminAccount)@$($domain)"
    } elseif($selection -eq "2"){
        Write-Host "Caching NSX Manager 1 credentials"
        $global:nsxcreds = Get-Credential -Message "Admin account to connect to NSX Manager 1" -UserName "$($NSXAdminAccount)@$($domain)"
    } elseif($selection -eq "3"){
        Write-Host "Caching ESXi Host credentials"
        $global:esxihostcreds = Get-Credential -Message "Admin account to connect to ESXi Host" -UserName "root"
    } elseif($selection -eq "4"){
        Write-Host "Caching All credentials"
        $global:vccreds = Get-Credential -Message "Admin account to connect to VCSA 1" -UserName "$($adminAccount)@$($domain)"
        $global:nsxcreds = Get-Credential -Message "Admin account to connect to NSX Manager 1" -UserName "$($adminAccount)@$($domain)"
        $global:esxihostcreds = Get-Credential -Message "Admin account to connect to ESXi Host" -UserName $ESXiAdminAccount
    } elseif($selection -eq "q"){
        break
    } else {
        Write-Host "Invalid Selection"
    }
}

function clearCredentials{
    Write-Host "Clearing all credentials"
    $global:vccreds = $null
    $global:nsxcreds = $null
    $global:esxihostcreds = $null
}

function init{
    Write-Host "Importing modules"
    $date1 = $(Get-Date)
    dir $pathtomodules | Unblock-File
    Write-Host "Importing PowerNSX"
    Import-Module "$($pathtomodules)\PowerNSX.psd1"
    Import-Module "$($pathtomodules)\PowerNSX.psm1"
    Write-Host "Importing Vmware.VimAutomation.Core"
    Import-Module VMware.VimAutomation.Core
    Write-Host "Importing Vmware.VimAutomation.Vds"
    Import-Module VMware.VimAutomation.Vds
    Write-Host "Importing Vmware.VimAutomation.Storage"
    Import-Module VMware.VimAutomation.Storage
    $date2 = $(Get-Date)
    $time = $date2 - $date1
    Write-Host "Importing modules took $($time.seconds) seconds."
}

function servers {
    if(!$global:DefaultVIServers){
        Write-Host "You are not connected to any vCenter Servers"
    } else {
        foreach($server in $global:DefaultVIServers){
            Write-Host "you are connected to $($server.Name)"
        }
    }
}

function vcc{
    if($global:DefaultVIServers){
        Write-Host "you are currently connected to one or more vcenters"
        servers
        $reponse = $null
        while(($response -ne "y") -and ($response -ne "yes") -and ($response -ne "n") -and ($response -ne "no")){
            $response = Read-Host "if you are connected to the correct vcenter enter y or yes else enter n or no"
        }
        if(($response -eq "n") -or ($response -eq "no")){
            Disconnect-VIServer * -Confirm:$false -WarningAction SilentlyContinue
            $selection = $null
            while(($selection -ne "1") -and ($selection -ne "2") -and ($selection -ne "3") -and ($selection -ne "4") -and ($selection -ne "q")){
                Write-Host "1 to connect to to vCenter 1"
                Write-Host "2 to connect to to vCenter 2"
                Write-Host "3 to connect to to vCenter 3"
                Write-Host "4 to connect to to vCenter 4"
                Write-Host "q to quit"
                $selection = Read-Host "Select which vCenter you would like to connect to"
            }
            if($selection -eq "1"){
                Write-Host "Connecting to vCenter 1"
                $vc = Connect-VIServer "$($vcsa1).$($domain)" -Credential $global:vccreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
                servers
            } elseif($selection -eq "2"){
                Write-Host "Connecting to vCenter 2"
                $vc = Connect-VIServer "$($vcsa2).$($domain)" -Credential $global:vccreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
                servers
            } elseif($selection -eq "3"){
                Write-Host "Connecting to vCenter 3"
                $vc = Connect-VIServer "$($vcsa3).$($domain)" -Credential $global:vccreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
                servers
            } elseif($selection -eq "4"){
                Write-Host "Connecting to vCenter 4"
                $vc = Connect-VIServer "$($vcsa4).$($domain)" -Credential $global:vccreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
                servers
            } elseif($selection -eq "q"){
                break
            } else {
                Write-Host "Invalid Selection"
            }
        }
    } else {
        $selection = $null
        while(($selection -ne "1") -and ($selection -ne "2") -and ($selection -ne "3") -and ($selection -ne "4") -and ($selection -ne "q")){
            Write-Host "1 to connect to to vCenter 1"
            Write-Host "2 to connect to to vCenter 2"
            Write-Host "3 to connect to to vCenter 3"
            Write-Host "4 to connect to to vCenter 4"
            Write-Host "q to quit"
            $selection = Read-Host "Select which vCenter you would like to connect to"
        }
        if($selection -eq "1"){
            Write-Host "Connecting to vCenter 1"
            $vc = Connect-VIServer "$($vcsa1).$($domain)" -Credential $global:vccreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
            servers
        } elseif($selection -eq "2"){
            Write-Host "Connecting to vCenter 2"
            $vc = Connect-VIServer "$($vcsa2).$($domain)" -Credential $global:vccreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
            servers
        } elseif($selection -eq "3"){
            Write-Host "Connecting to vCenter 3"
            $vc = Connect-VIServer "$($vcsa3).$($domain)" -Credential $global:vccreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
            servers
        } elseif($selection -eq "4"){
            Write-Host "Connecting to vCenter 4"
            $vc = Connect-VIServer "$($vcsa4).$($domain)" -Credential $global:vccreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
            servers
        } elseif($selection -eq "q"){
            break
        } else {
            Write-Host "Invalid Selection"
        }
    }
}

function vcd{
    if($global:DefaultVIServers){
        Write-Host "Disconnecting from vCenter servers"
        Disconnect-VIServer * -Confirm:$false
    }
    servers
}

function nsxc{
    if($global:defaultnsxconnection){
        Write-Host "you are currently connected to one or more NSX managers"
        $global:defaultnsxconnection.Server
        $reponse = $null
        while(($response -ne "y") -and ($response -ne "yes") -and ($response -ne "n") -and ($response -ne "no")){
            $response = Read-Host "if you are connected to the correct NSX Managers enter y or yes else enter n or no"
        }
        if(($response -eq "n") -or ($response -eq "no")){
            Disconnect-NsxServer * -Confirm:$false -WarningAction SilentlyContinue
            $selection = $null
            while(($selection -ne "1") -and ($selection -ne "2") -and ($selection -ne "3") -and ($selection -ne "4") -and ($selection -ne "q")){
                Write-Host "1 to connect to to NSX manager 1"
                Write-Host "2 to connect to to NSX manager 2"
                Write-Host "3 to connect to to NSX manager 3"
                Write-Host "4 to connect to to NSX manager 4"
                Write-Host "q to quit"
                $selection = Read-Host "Select which NSX manager you would like to connect to"
            }
            if($selection -eq "1"){
                Write-Host "Connecting to NSX manager 1"
                $nsxm = Connect-NsxServer "$($nsxmgr1).$($domain)" -Credential $global:nsxcreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
                servers
            } elseif($selection -eq "2"){
                Write-Host "Connecting to NSX manager 2"
                $nsxm = Connect-NsxServer "$($nsxmgr2).$($domain)" -Credential $global:nsxcreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
                servers
            } elseif($selection -eq "3"){
                Write-Host "Connecting to NSX manager 3"
                $nsxm = Connect-NsxServer "$($nsxmgr3).$($domain)" -Credential $global:nsxcreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
                servers
            } elseif($selection -eq "4"){
                Write-Host "Connecting to NSX manager 4"
                $nsxm = Connect-NsxServer "$($nsxmgr4).$($domain)" -Credential $global:nsxcreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
                servers
            } elseif($selection -eq "q"){
                break
            } else {
                Write-Host "Invalid Selection"
            }
        }
    } else {
        $selection = $null
        while(($selection -ne "1") -and ($selection -ne "2") -and ($selection -ne "3") -and ($selection -ne "4") -and ($selection -ne "q")){
            Write-Host "1 to connect to to NSX manager 1"
            Write-Host "2 to connect to to NSX manager 2"
            Write-Host "3 to connect to to NSX manager 3"
            Write-Host "4 to connect to to NSX manager 4"
            Write-Host "q to quit"
            $selection = Read-Host "Select which NSX manager you would like to connect to"
        }
        if($selection -eq "1"){
            Write-Host "Connecting to NSX manager 1"
            $nsxm = Connect-NsxServer "$($nsxmgr1).$($domain)" -Credential $global:nsxcreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
            servers
        } elseif($selection -eq "2"){
            Write-Host "Connecting to NSX manager 2"
            $nsxm = Connect-NsxServer "$($nsxmgr2).$($domain)" -Credential $global:nsxcreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
            servers
        } elseif($selection -eq "3"){
            Write-Host "Connecting to NSX manager 3"
            $nsxm = Connect-NsxServer "$($nsxmgr3).$($domain)" -Credential $global:nsxcreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
            servers
        } elseif($selection -eq "4"){
            Write-Host "Connecting to NSX manager 4"
            $nsxm = Connect-NsxServer "$($nsxmgr4).$($domain)" -Credential $global:nsxcreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
            servers
        } elseif($selection -eq "q"){
            break
        } else {
            Write-Host "Invalid Selection"
        }
    }
}

function nsxd{
    if($global:defaultnsxconnection){
        Write-Host "Disconnecting from NSX managers"
        Disconnect-NsxServer * -Confirm:$false
        Write-Host "Disconnected from NSX managers"
    }
}

function getCertXMLHost{
    param([parameter(Mandatory=$true)][string] $Destination, [parameter()][int] $Port=443)
    try{
        $client = New-Object Net.sockets.tcpclient
        $client.connect($Destination, $Port)
        $stream = $client.GetStream()
        $callback = {Param($sender, $cert, $chain, $errors) return $true}
        $sslstream = [System.Net.Security.SslStream]::new($stream, $true, $callback)
        $sslstream.AuthenticateAsClient('')
        [System.Security.Cryptography.X509Certificates.X509Certificate2]::new($sslstream.RemoteCertificate)
    } catch {
        Write-Warning $_
    } finally {
        $sslstream.Dispose()
        $client.Dispose()
    }
}

function getESXiHostCertificate{
    [CmdletBinding()]param($hostname, $expyear)
    $cert = getCertXMLHost $hostname
    $expDate = $cert.notafter
    $certCN = $cert.subject
    if($expyear){
        $hdate = (((($expDate.tostring()).split(' '))[0]).split('/'))[2]
        if([int]$hdate -lt [int]$expyear){
            Write-Host "$($hostname) expires $($expDate)"
        }
        if($certCN -match "VMware Engineering"){
            Write-Host "$($hostname) has a self signed certificate"
        }
    } else {
        Write-Host "$($hostname) expires $($expDate)"
    }
}

function getExpiringESXiHostCertificates{
    [CmdletBinding()]param($hostname, $expyear)
    vcc
    Write-Host "Starting to obtain host certificate"
    if($hostname){
        if($expyear){
            getESXiHostCertificate -hostname $hostname -expyear $expyear
        } else {
            getESXiHostCertificate -hostname $hostname
        }
    } else {
        $hosts = Get-VMHost
        foreach($curr in $hosts){
            if($expyear){
                getESXiHostCertificate -hostname $curr.name -expyear $expyear
            } else {
                getESXiHostCertificate -hostname $curr.name
            }
        }
    }
    Write-Host "Completed to obtain host certificate"
}

function replaceHostCertificates{
    [CmdletBinding()]param($hostname, $clustername)
    vcc
    Write-Host "Starting to replace host certificates"
    if($hostname){
        $hostlist = $hostname
    } elseif($clustername) {
        $hostlist = ((Get-Cluster -name $clustername | Get-VMHost | Where-Object{$_.Name -notmatch "hostname"}))
        $hostlist = ($hostlist).name
    } else {
        $response = $null
        while(($response -ne "y") -and ($response -ne "yes") -and ($response -ne "n") -and ($response -ne "no")){
            $response = Read-Host "Certificates will be replaced for ALL hosts in current vCenter, if you want this enter y or yes else n or no"
        }
        if(($response -eq "y") -or ($response -eq "yes")){
            $hostlist = Get-VMHost | Where-Object{$_.Name -notmatch "hostname"}
            $hostlist = ($hostlist).name
        } else {
            Write-Host "Do not want to continue operation on entire vCenter"
            break
        }
    }
    
    $response = $null
    while(($response -ne "y") -and ($response -ne "yes") -and ($response -ne "n") -and ($response -ne "no")){
        $response = Read-Host "Please ensure the rui.crt, rui.key and castore.pem are in the right directories, if you would like to continue y or yes else n or no"
    }
    if(($response -eq "y") -or ($response -eq "yes")){
        $crtPath = "$($certDirectory)\$($g)\rui.crt"
        $keyPath = "$($certDirectory)\$($g)\rui.key"
        $pemPath = "$($certDirectory)\castore.pem"
        $uriHostKey = "https://$($h)/host/ssl_key"
        $uriHostCrt = "https://$($h)/host/ssl_cert"
        $uriHostPem = "https://$($h)/host/castore"
        Invoke-WebRequest -uri $uriHostKey -Method Put -InFile $keyPath -ContentType 'application/pkcs8' -Credential $global:esxihostcreds
        Invoke-WebRequest -uri $uriHostCrt -Method Put -InFile $crtPath -ContentType 'application/x-x509-ca-cert' -Credential $global:esxihostcreds
        Invoke-WebRequest -uri $uriHostPem -Method Put -InFile $pemPath -ContentType 'application/x-pem-file' -Credential $global:esxihostcreds
    } else {
        Write-Host "Please ensure files are in correct directories with correct names"
        break
    }
}

function findVM{
    [CmdletBinding()]param($vm)
    vcc
    $listofhosts = (Get-VMHost).name
    vcd
    foreach($h in $listofhosts){
        $session = Connect-VIServer $h -Credential $global:esxihostcreds -WarningAction SilentlyContinue -ErrorAction SilentlyContinue
        Write-Host -NoNewline "$($h) "
        Write-Host "$(Get-VM | Where-Object{$_.Name -match $vm})" -fore Green
        Disconnect-VIServer $session -Confirm:$false
    }
}

function attachVIB{
    [CmdletBinding()]param($vibname)
    vcc
    Write-Host "Attaching VIB $vibname to all hosts in connected vCenter"
    $baseline = Get-Baseline -name $vibname
    $hosts = Get-VMHost
    Attach-Baseline -Entity $hosts -Baseline $baseline
    Write-Host "Scanning hosts for compliance"
    Scan-Inventory -Entity $hosts
    Write-Host "completed attaching VIB and scanning hosts for compliance"
}

function patchVIB{
    [CmdletBinding()]param($host, $cluster, $vibname)
    vcc
    if(!$global:DefaultVIServers){
        Write-Host "You are not connected to a vCenter, please try again."
        break
    }
    if($host){
        $toRemediate = $host
        Write-Host "Remediating on Host $host"
    } elseif($cluster){
        $toRemediate = $cluster
        Write-Host "Remediating on Cluster $cluster"
    } else {
        $toRemediate = Get-Datacenter | Get-VMHost
        Write-Host "Remediating on Datacenter $((Get-Datacenter).name)"
    }
    $baseline = Get-Baseline -Name $vibname
    Remediate-Inventory -Entity $toRemediate -Baseline $baseline
    Write-Host "Completed remediating $($toRemediate)"
}



























